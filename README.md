## Manual de configuração do ambiente de desenvolvimento

### 1) Permitir usar sudo sem informar a senha 
```
 sudo nano /etc/sudoers.d/nome_do_meu_usuario_da_maquina
```
Conteúdo que deve ser inserido no arquivo
```
nome_do_meu_usuario_da_maquina ALL=(ALL:ALL) NOPASSWD:ALL
```
Salve o arquivo e execute o comando abaixo, para verificar se a sintaxe está correta.
```
 visudo -c -f /etc/sudoers
```
### 2) Instalação do ansible
```
 sudo apt update
```
```
 sudo apt install ansible
``` 

### 3) Instalação e configuração do git
> Obs: Caso já tenha o git instalado e configurado em seu computador, você poderá ignorar esta etapa.

Instalação do git
```
 sudo apt install git
``` 
```
 git config --global user.name "Seu nome"
 git config --global user.email "seuemail@email.com.br"
```

### 4) Execução do playbook com o setup inicial

Para clonar o repositório do playbook, execute o seguinte comando em seu terminal.
> Obs: Defina o diretório que deseja clonar o repositório e navegue até o mesmo, antes de executar o comando abaixo.
```
 git clone git@bitbucket.org:brumalmeida/setup-inicial.git
```
```
 cd setup-inicial && ansible-playbook initial.setup.yml
```